###### Copyright (C) 2024 aLean-Tec, Inc. 
###### CERN Open Hardware Licence Version 2 - Permissive

#### DISCLAIMER: this project/repository is made available solely for educational purposes. 

# Kria260 Computer Architecture labs

The labs in this repository are part cover different aspects of computer architecture by configuring an AMD Kria KV260 Vision AI Starter Kit development board platform. 
This Kria SOM (System on Module) is a Multi-Processor System-On-a-Chip (SoC) Field Programmable Gate Array (FPGA) development board.  The KV260 is based on Xilinx’s Zynq® UltraScale+™ MPSoC hardware architecture and ARM’s Cortex A53 software stack. The Kria KV260 SOM development boards available for this lab are running Ubuntu 22.04 LTS and will be customized using various software development tools such as Vivado, Vitis and Linux. 

## Support
Please open an issue in the this GitLab repository.

## Project status
This project is in its inital phase.
