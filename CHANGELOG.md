###### Copyright (C) 2024 aLean-Tech, Inc. 
###### CERN Open Hardware Licence Version 2 - Permissive

# Change Log
All notable changes to this project will be documented in this file. Including: Recommended groupings for changes include: 
```diff
+ Added, + Changed, + Deprecated, + Removed, + Fixed, + Security
 +
```
The format is based on [Keep a Changelog](http://keepachangelog.com/).

## *[UNVERSIONED]* 2024-April-30
### Added
- README.md from default template, project creation
- LICENSE.md CERN Open Hardware Licence Version 2 - Permissive
- CHANGELOG.md (this file)
- CONTRIBUTING.md
- CONTRIBUTORS.md
- Badges for license, version-tag and governance
