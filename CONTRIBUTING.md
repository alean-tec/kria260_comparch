###### Copyright (C) 2024 aLean-Tec, Inc.
###### CERN Open Hardware Licence Version 2 - Permissive

# Contributing
This project is public and licensed under a [CERN Open Hardware Licence Version 2 - Permissive](https://ohwr.org/cern_ohl_p_v2.txt) license. You have the freedom to use, study, modify, share and distribute this project under those conditions. 

Please consider discussing the change you wish to make by creating an issue on this repository; it may save you some time and effort. 

1. It is recommended that you refer to the [Git SCM](https://git-scm.com) webpage to chose the git client sw you will use. Out instructions assume you are using [gitbash](https://www.git-scm.com/book/en/v2/Appendix-A%3A-Git-in-Other-Environments-Git-in-Bash) or its equivalent.

2. Make sure you have configured your [Personal Access Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) or [SSH Key ](https://docs.gitlab.com/ee/user/ssh.html) prior to submitting your contribution. 

3. Get a copy of the project and change its default alias to "upstream":
```
git clone https://gitlab.com/alean-tec/kria260_comparch.git

git cd kria260_comparch
git remote -v
git remote rename origin upstream
git checkout main
```

4. add your own repository under the defaul alias "origin" and save there the copy of the "upstream" repository:
```
git remote add origin <<-YOUR-REPOSITORY->>
git push origin main
```

5. each time you may want to synchronize your copy of the repository with the "upstream" repository:
```
git fetch upstream
git checkout main
git rebase upstream/main
git push -f origin main
```

6. If you want to prepare some changes to merge with the "upstream" repository, first creat a branch and later create a merge request:
```
git checkout -b <<-MY-BRANCH-NAME->>
# add any new files or modify existing source
git add <<-MY-NEW-OR-UPDATED-FILE->> 
git commit -m "My great contriution to this project is ..." -s
git push origin <<-MY-BRANCH-NAME->>
```

7. Ensure any install or build dependencies are removed before pushing the changes.

8. Update the necessary README.md files with details of the changes; this includes new environment variables, references to Issues, useful file locations and parameters needed to invoque a build that will use the changes (helps for debugging or studying the change).

9. Increase the version numbers in the CHANGELOG.md, any examples files and the README.md to the new version that this Pull Request would represent. The versioning scheme we use is [SemVer](http://semver.org/).

10. All contributions must be done using a merge request to allow for review.

## Code of Conduct
In the interest of fostering an open and welcoming environment, we as contributors and maintainers pledge to making participation in our project and our community a harassment-free experience for everyone, regardless of age, body size, disability, ethnicity, gender identity and expression, level of experience, nationality, personal appearance, race, religion, or sexual identity and orientation.

### Our Collaborative space 
Examples of behavior that contributes to creating a positive environment include:
* Using welcoming and inclusive language
* Being respectful of differing viewpoints and experiences
* Gracefully accepting constructive criticism
* Focusing on what is best for the community
* Showing empathy towards other community members

Examples of unacceptable behavior by participants include:
* The use of sexualized language or imagery and unwelcome sexual attention or advances
* Trolling, insulting/derogatory comments, and personal or political attacks 
* Public or private harassment
* Publishing others' private information, such as a physical or electronic   address, without explicit permission
* Other conduct which could reasonably be considered inappropriate in a   professional setting

### Scope
This pledge applies both within project spaces and in public spaces when an individual is representing the project or its community. Examples of representing a project or community include using an official project e-mail address, posting via an official social media account, or acting as an appointed representative at an online or offline event. Representation of a project may be further defined and clarified by project maintainers.

### Attribution
This Code of Conduct is adapted from the [Contributor Covenant](https://www.contributor-covenant.org/version/1/4/code-of-conduct/)
