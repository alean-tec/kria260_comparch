###### Copyright (C) 2024 aLean-Tec, Inc. 
###### CERN Open Hardware Licence Version 2 - Permissive

## Kria260 Computer Architecture labs contributors (sorted alphabetically)

  
#### [Alfredo Herrera](https://gitlab.com/alfredoh1234)
* Technical lead (default maintainer)
* Project lead (default reporter)

### [Full contributors list](https://gitlab.com/alean-tech/kria260_comparch/-/project_members)
